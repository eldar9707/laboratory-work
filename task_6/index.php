<?php

function is_arithmetic($arr): ?string
{
    $delta = $arr[1] - $arr[0];
    for($index=0; $index<sizeof($arr)-1; $index++)
    {
        if (($arr[$index + 1] - $arr[$index]) != $delta)
        {

            return null;
        }
    }
    return $delta;
}

$array = [];

for ($i = 0; $i < 4; $i++) {
    $array[] = intval(trim(fgets(STDIN)));
}

is_arithmetic($array) == null ? null : print "Difference Progression: " . is_arithmetic($array)."\n";